/**
 * Created by Kevin on 5/03/2017.
 */
function renderHttpResponse(data, statuscode){
    const response = {
        statusCode: statuscode||200,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': '*',
            'Access-Control-Allow-Methods': 'POST, GET, PUT, DELETE, OPTIONS',
            'Content-Type': 'application/json'
        },
        body: typeof data === 'object' ? JSON.stringify(data):data
    }
    return response
}

function renderHttpError(error) {
    const statusCode = 400
    var response
    if (error) {
        response = typeof error === Error ? error.message : error
    }
    try {
        response = JSON.stringify(response)
    } catch (e) {
        response = `${response}`
    }
    return renderHttpResponse(response, statusCode)
}

// Handle the Lambda API Gateway response, convert the result to string
function respondToLambdaRoute(callback, response, error){
    const statusCode = error ? 500 : 200
    if (error) {
        response = typeof error === Error ? error.message : error
    }
    try {
        response = JSON.stringify(response)
    } catch (e) {
        response = `${response}`
    }
    // Add cross-origin headers to the response to support browser ajax requests
    const apiGatewayResponse = {
        statusCode,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': '*',
            'Access-Control-Allow-Methods': 'POST, GET, PUT, DELETE, OPTIONS',
            'Content-Type': 'application/json'
        },
        body: response
    }
    callback(null, apiGatewayResponse)
}

export {renderHttpError, renderHttpResponse, respondToLambdaRoute}