/**
 * Created by Kevin on 5/03/2017.
 */
const uuidV4 = require('uuid/v4')
const S3 = require('aws-sdk').S3
const s3Config = require('../config.json').s3

const s3 = new S3({ signatureVersion: 'v4' })

const BUCKET_NAME = s3Config.bucketName
const BUCKET_FOLDER = s3Config.folder
const PHOTO_TYPES = ['jpg','jpeg','png','gif']// Allowed photo upload extensions,
//Regex with parameter 'g' doesn't work on the node version that lambda is running, it is a known bug
//Instead we use a simple array and check it with the indexOf

function sign(filename, contentType){
    const params = {
        Bucket: BUCKET_NAME,
        Key: '',
        Expires: 120,
        ContentType: ''
    };

    const regex = /(?:\.([^.]+))?$/;
    let extension = regex.exec(filename)[1]
    return new Promise(function(resolve,reject){
        if(extension==='undefined')
            reject(_getErrorResp('no extension'))
        if(PHOTO_TYPES.indexOf(extension)===-1)
            reject(_getErrorResp('invalid extension'))
        s3.getSignedUrl('putObject', Object.assign({},params,{
            Key:`${BUCKET_FOLDER}${uuidV4()}.${extension}`,
            ContentType: contentType
        }), function(err, data) {
            if (err) {
                console.log(err);
                reject(_getErrorResp(err))
            } else {
                resolve(_getUrlResp(data))
            }
        });
    })


}


function _getErrorResp(err){
    return {error: err}
}
function _getUrlResp(url){
    return {url: url}
}

module.exports = {sign}