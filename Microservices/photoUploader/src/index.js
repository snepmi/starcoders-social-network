/**
 * Created by Kevin on 5/03/2017.
 */
const photoHandler = require('./lib/photoHandler')
const {renderHttpError,renderHttpResponse} = require('./lib/responseHandler')
function handler (event, context, callback){
    console.log('Lambda params:', JSON.stringify(event))

    if (event.httpMethod) {
        // Imediatelly respond to an OPTIONS call with an empty response body
        // for browser HTTP cross-domain ajax calls
        if (event.httpMethod === 'OPTIONS') {
            return respondToLambdaRoute(callback, {})
        }


        if(event.httpMethod === 'POST' && event.resource === '/v1/photos/sign') {
            if(!event.body)
                return callback(null,  renderHttpError({error: 'No body'}))

            //parse the body to an object
            const body = JSON.parse(event.body)
            //check if there is a filename in the body
            if(!body.filename)
                return callback(null,  renderHttpError({error: 'No filename in body'}))
            //check if there is a contentType in the body
            if(!body.contentType)
                return callback(null,  renderHttpError({error: 'No contentType in body'}))

            //Get the filename
            const filename = body.filename
            const contentType = body.contentType
            //Get a signed url
            photoHandler.sign(filename, contentType)
                .then((data)=>{
                    callback(null, renderHttpResponse(data))
                }, (error)=>{
                    callback(null, renderHttpResponse(error))
                })
                .catch((err)=>{
                    callback(null,renderHttpError(err))
                })
        }
        else
            respondToLambdaRoute(callback, null, `Unknown route '${route}'.`)


    }
}

module.exports = {handler}



