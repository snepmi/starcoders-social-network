/**
 * Created by Kevin on 5/03/2017.
 */
export const apiEndPoint = 'http://localhost:3001'
export const usersEndpoint = `${apiEndPoint}/users`
export const usersFindIdEndpoint = `${apiEndPoint}/users/find`

export const createUser = `${apiEndPoint}/users`

export const allPhotos = `${apiEndPoint}/photos`



