import React, { PropTypes, Component} from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as Actions from '../reducers/actions'

//Import components
import Header from '../components/Header'
import Footer from '../components/Footer'

import '../styles/content.css';

class App extends Component {
    constructor(props){
        super()
        props.actions.checkLogin()
    }
  render() {
    return (
      <div className="App">
        <Header/>
          <div className="content">
              {this.props.children}
          </div>

        <Footer/>

      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
    return { actions: bindActionCreators(Actions, dispatch) }
}

export default connect(
    state => state,
    mapDispatchToProps
)(App)
