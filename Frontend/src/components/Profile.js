/**
 * Created by Kevin on 7/03/2017.
 */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Dropzone from 'react-dropzone'

import * as Actions from '../reducers/actions'

import AuthService from '../services/AuthService'

class Profile extends Component{

    constructor(props){
        super()
    }

    componentWillMount(){
        this.props.actions.getProfile(AuthService.getId())
    }

    onDrop(acceptedFiles, rejectedFiles) {
        console.log('Accepted files: ', acceptedFiles);
        console.log('Rejected files: ', rejectedFiles);
    }

    render(){
        return(
            <div id="profile">
                <h2>{this.props.userProfile.name}</h2>
                <h4>Description</h4>
                <p id="description">{this.props.userProfile.decription}</p>
                <h4>Photos</h4>
                <Dropzone onDrop={this.onDrop}>
                    <div>Try dropping some files here, or click to select files to upload.</div>
                </Dropzone>

            </div>
        )
    }

}

function mapDispatchToProps(dispatch) {
    return { actions: bindActionCreators(Actions, dispatch) }
}

export default connect(
    state => state,
    mapDispatchToProps
)(Profile)