/**
 * Created by Kevin on 6/03/2017.
 */
import React,{Component} from 'react'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import * as Actions from '../reducers/actions'
import {Link} from 'react-router'

import '../styles/photoDetail.css'

import AuthService from '../services/AuthService'
import * as endpoints from '../constants/endpoints'

import PhotoDetailComment from './PhotoDetailComment'

class PhotoDetail extends Component{

    constructor(props){
        super()
        this.state = {
            photo: {
                title: '',
                url: '',
                Comments: [],
                Likes: [],
                User:{
                    id: 0,
                    name: ''
                }
            }
        }

        this.likePhoto = this.likePhoto.bind(this)


    }




    componentWillMount(){
        const photo_id = this.props.params.photoId
        const user_id = this.props.params.userId
        this.props.actions.getPhotoDetail(user_id,photo_id)

    }

    renderComment(){
        return this.props.photoDetail.Comments.map((comment,index)=>{
            return(
                <PhotoDetailComment key={index} comment={comment} user_id={this.props.photoDetail.user_id} photo_id={this.props.photoDetail.id} addMode={Object.keys(comment).length === 0?true:false}></PhotoDetailComment>
            )
        })
    }

    checkIfLike(){
        console.log(this.props.photoDetail.Likes)
        return this.props.photoDetail.Likes.filter((like)=>{
            return like.user_id === AuthService.getId();

        })
    }

    likePhoto(){
        const photo_id = this.props.params.photoId
        const user_id = this.props.params.userId
        this.props.actions.likePhoto(user_id,photo_id)
    }

    render(){
        let currentUserLikes = this.checkIfLike()
        let likeClass = currentUserLikes.length!=0?'fa fa-thumbs-up fa-3x user-like':'fa fa-thumbs-up fa-3x '
        return(
            <div id="photo-detail">
                <h3><Link to={`/users/${this.props.photoDetail.User.id}`}>{this.props.photoDetail.User.name}</Link></h3>
                <div id="likes">
                    <span>{this.props.photoDetail.Likes.length}</span>
                    <i className={likeClass} aria-hidden="true" onClick={this.likePhoto}></i>
                </div>
                <img src={this.props.photoDetail.url} alt={this.props.photoDetail.title}/>
                <div className="comments">
                    {this.renderComment()}
                </div>
            </div>
        )
    }
}
function mapDispatchToProps(dispatch) {
    return { actions: bindActionCreators(Actions, dispatch) }
}

export default connect(state=>state,mapDispatchToProps)(PhotoDetail)