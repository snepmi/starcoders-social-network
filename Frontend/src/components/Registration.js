/**
 * Created by Kevin on 6/03/2017.
 */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as Actions from '../reducers/actions'

import RegisterUserProfile from './RegisterUserProfile'
import RegisterPhotosUpload from './RegisterPhotosUpload'

class Registration extends Component{

    constructor(props){
        super()
        this.state = {
            registrationSteps: 2,
            currentStep: 0,
            steps:[
                (<RegisterUserProfile/>),
                (<RegisterPhotosUpload/>)
            ]
        }

        this.nextStep = this.nextStep.bind(this)
    }

    nextStep(){
        if(this.state.currentStep++ != this.state.registrationSteps){
            this.setState = {
                currentStep: this.state.currentStep++
            }
        }

    }

    render(){
        return(
            <div id="registration">
                {this.state.steps[this.state.currentStep]}
                <div className="btn" onClick={this.nextStep}></div>
            </div>
        )
    }

}

function mapDispatchToProps(dispatch) {
    return { actions: bindActionCreators(Actions, dispatch) }
}

export default connect(
    state => state,
    mapDispatchToProps
)(Registration)