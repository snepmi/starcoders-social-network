/**
 * Created by Kevin on 6/03/2017.
 */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as Actions from '../reducers/actions'

class RegisterUserProfile extends Component{

    constructor(){
        super()
    }

    render(){
        return(
            <div id="user-profile">
                <h4>user profile</h4>
            </div>
        )
    }

}

function mapDispatchToProps(dispatch) {
    return { actions: bindActionCreators(Actions, dispatch) }
}

export default connect(
    state => state,
    mapDispatchToProps
)(RegisterUserProfile)