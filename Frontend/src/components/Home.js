/**
 * Created by Kevin on 5/03/2017.
 */
import React,{Component} from 'react'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import * as Actions from '../reducers/actions'
import { Link } from 'react-router'
import '../styles/homeAuth.css'

class Home extends Component{

    renderPhotosList(){
        console.log(this.props)
        return this.props.photos.map((photo,index)=>{
            return (<li key={index}>
                <Link to={`/users/${photo.User.id}/photo/${photo.id}`}>
                    <div>
                        <img src={photo.url} alt={photo.title}/>
                        <p>{photo.title}</p>
                    </div>
                </Link>



            </li>)
        })
    }

    renderPublic(){
        return(
            <h2>public</h2>
        )
    }

    renderLoggedIn(){
        return(
            <div className="photos">
                <ul id="photo-list">
                    {this.renderPhotosList()}
                </ul>
            </div>
        )
    }

    render(){
        return(
            <div className="content">
                {!this.props.isAuthenticated?this.renderPublic():this.renderLoggedIn()}
            </div>
        )
    }
}
function mapDispatchToProps(dispatch) {
    return { actions: bindActionCreators(Actions, dispatch) }
}

export default connect(state=>state,mapDispatchToProps)(Home)