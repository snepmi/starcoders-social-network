/**
 * Created by Kevin on 7/03/2017.
 */
import React,{Component,PropTypes} from 'react'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import * as Actions from '../reducers/actions'
import {Link} from 'react-router'

import '../styles/homeAuth.css'


import * as endpoints from '../constants/endpoints'

class PhotoDetailComment extends Component{

    constructor(props){
        super()
        this.state = {
            comment: ''
        }

        this.renderAdd = this.renderAdd.bind(this)
        this.renderComment = this.renderComment.bind(this)
        this.commentOnChange = this.commentOnChange.bind(this)
        this.commentSubmit = this.commentSubmit.bind(this)
    }

    commentOnChange(ev){
        const value = ev.target.value

        this.setState({
            comment: value
        })
    }


    commentSubmit(ev) {
        if (ev.key === 'Enter' && this.state.comment != '') {

            console.log(this.props.comment)
            this.props.actions.addComment(this.props.user_id, this.props.photo_id, this.state.comment)
        }
    }


    renderAdd(){
        return(
            <div className="comment">
                <input type="text" value={this.state.comment} onChange={this.commentOnChange} onKeyPress={this.commentSubmit} placeholder="Add a comment"/>
            </div>
        )
    }

    renderComment(){
        return(
            <div className="comment">
                <p><Link to={`${endpoints}/users/${this.props.comment.user_id}`}>{this.props.comment.User.name}</Link></p>
                <p>{this.props.comment.content}</p>
            </div>
        )
    }


    render(){
        return (
            this.props.addMode?this.renderAdd():this.renderComment()
        )
    }
}


function mapDispatchToProps(dispatch) {
    return { actions: bindActionCreators(Actions, dispatch) }
}

export default connect(state=>state,mapDispatchToProps)(PhotoDetailComment)