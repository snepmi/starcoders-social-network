/**
 * Created by Kevin on 5/03/2017.
 */
import React, {Component} from 'react'
import {connect} from 'react-redux'

import '../styles/footer.css'

class Footer extends Component{

    render(){
        return(
            <footer>
                <p>&copy;Kevin Impens - The Social Network</p>
            </footer>
        )
    }
}

export default connect()(Footer)

