/**
 * Created by Kevin on 6/03/2017.
 */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as Actions from '../reducers/actions'

class RegisterPhotosUpload extends Component{

    constructor(){
        super()

    }


    render(){
        return(
            <div id="photos-upload">
                <h4>photo upload</h4>
            </div>
        )
    }

}

function mapDispatchToProps(dispatch) {
    return { actions: bindActionCreators(Actions, dispatch) }
}

export default connect(
    state => state,
    mapDispatchToProps
)(RegisterPhotosUpload)