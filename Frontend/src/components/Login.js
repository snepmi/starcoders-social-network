/**
 * Created by Kevin on 6/03/2017.
 */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as Actions from '../reducers/actions'

import AuthService from '../services/AuthService'

class Login extends Component {
    constructor(){
        super()

    }

    componentDidMount(){
        this.props.actions.loginRequest()
    }

    render() {
        return false
    }
}


function mapDispatchToProps(dispatch) {
    return { actions: bindActionCreators(Actions, dispatch) }
}

export default connect(
    state => state,
    mapDispatchToProps
)(Login)
