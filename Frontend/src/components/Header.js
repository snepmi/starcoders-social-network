/**
 * Created by Kevin on 5/03/2017.
 */
import React, { PropTypes, Component} from 'react'
import { connect } from 'react-redux'
import {Link} from 'react-router'

import Search from '../components/Search'
import '../styles/header.css'

class Header extends Component{

    isAuth(){
        return !this.props.isAuthenticated ? (
                <Link to="/login" className="btn primary" id="login">Login</Link>
            ) : (
                <div id="loggedIn">
                    <span>Welcome, <Link to="/profile">{this.props.profile.nickname}</Link></span>
                    <Link to="/logout" className="btn primary" id="logout">Logout</Link>
                </div>
            )
    }

    render(){
        return(
            <header>
                <div className="header">
                    <h1>The Social Network</h1>
                    <Search/>
                    {this.isAuth()}
                </div>

            </header>
        )
    }
}



export default connect(
    state => state
)(Header)
