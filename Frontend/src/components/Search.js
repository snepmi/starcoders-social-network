/**
 * Created by Kevin on 5/03/2017.
 */
import React,{Component} from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from '../reducers/actions'

class Search extends Component{

    constructor(){
        super()
        this.state = {
            search: "",
            focus: false
        }
        this.searchOnChange = this.searchOnChange.bind(this)
    }

    searchOnChange(ev){
        const search = ev.target.value
        this.setState({ search: search })
        this.props.actions.searchUsers(search, true)
    }

    onBlur(ev){
        console.log('losefucus')
        this.setState({
            focus: false
        })
    }

    onFocus(ev){
        console.log('focus')
        this.setState({
            focus: true
        })
        this.props.actions.searchUsers(null, true)
    }

    renderSearchResults(){
        return this.props.searchResults.map((user,index)=>{
            return(
                <li key={index}>{user.name}</li>
            )
        })
    }

    render(){
        return(
            <div className="search">
                <input type="text" value={this.state.search} onChange={this.searchOnChange} onBlur={this.onBlur.bind(this)} onFocus={this.onFocus.bind(this)}/>
                <ul id="searchResults" className={this.state.focus?'focus':'loseFocus'}>
                    {this.renderSearchResults()}
                </ul>
            </div>
        )
    }
}


function mapDispatchToProps(dispatch) {
    return { actions: bindActionCreators(Actions, dispatch) }
}

export default connect(
    state => state,
    mapDispatchToProps
)(Search)