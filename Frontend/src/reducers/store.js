/**
 * Created by Kevin on 5/03/2017.
 */
import reducer from "./reducer"
import {createStore, applyMiddleware, compose} from 'redux'
import thunkMiddleware from 'redux-thunk'

import AuthService from '../services/AuthService'

let INITIAL_STATE = {
    searchResults: [],

    isAuthenticated: !AuthService.isTokenExpired(),
    isFetching: false,
    profile: AuthService.getProfile(),
    error: null,

    userProfile: {},
    photos: [],
    photoDetail: {
        Comments: [],
        Likes: [],
        User: {}
    }
}

let middleWare = compose(
    applyMiddleware(thunkMiddleware)
)

let store = createStore(reducer,INITIAL_STATE, applyMiddleware(thunkMiddleware))
export default store