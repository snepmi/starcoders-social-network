/**
 * Created by Kevin on 5/03/2017.
 */
import * as actionTypes from '../constants/actionsTypes'
export default function reducer(state, action){
    let newState = JSON.parse(JSON.stringify(state))
    switch (action.type){
        case actionTypes.SEARCH_USERS_RECIEVED:{
            newState.searchResults  = action.users
            return newState
        }
        case actionTypes.LOGIN_REQUEST:
            return {...newState, isFetching: true, error: null}
        case actionTypes.LOGIN_SUCCESS:
            return {...newState, isFetching: false, isAuthenticated: true, profile: action.profile}
        case actionTypes.LOGIN_ERROR:
            return {...newState, isFetching: false, isAuthenticated: false, profile: {}, error: action.error}
        case actionTypes.LOGOUT_SUCCESS:
            return {...newState, isAuthenticated: false, profile: {}}
        case actionTypes.USER_CREATED: {
            newState = {...newState, id: action.user.id}
            return newState
        }
        case actionTypes.PHOTOS_RECIEVED:{
            newState.photos = action.photos
            return newState
        }
        case actionTypes.PHOTO_DETAIL_RECIEVED:{
                newState.photoDetail = action.photoDetail
                //Add empty comment, this is for the Add comment
                newState.photoDetail.Comments.push({})
            return newState
        }
        case actionTypes.PHOTO_DETAIL_ERROR_RECIEVED:{
                newState.error = action.err
            return newState
        }
        case actionTypes.COMMENT_RECIEVED:{
            //Remove the empty Add comment
            newState.photoDetail.Comments.pop()
            newState.photoDetail.Comments.push(action.comment)
            newState.photoDetail.Comments.push({})
            return newState
        }
        case actionTypes.PHOTO_LIKE_RECIEVED:{
            let likeInArray = newState.photoDetail.Likes.filter((like)=>{
                return like.photo_id===action.like.photo_id
            })
            if(likeInArray.length===0){
                newState.photoDetail.Likes.push(action.like)
            }else{
                newState.photoDetail.Likes = newState.photoDetail.Likes.filter((like)=>{
                    return like.photo_id!==action.like.photo_id

                })
            }

            return newState
        }
        case actionTypes.PHOTO_LIKE_ERROR_RECIEVED:{
            newState.error = action.err
            return newState
        }
        case actionTypes.PROFILE_RECIEVED:{
            newState.userProfile = action.profile
            return newState
        }
        case actionTypes.PROFILE_ERROR_RECIEVED:{
            newState.error = action.err
            return newState
        }
        default:
            return state
    }
}