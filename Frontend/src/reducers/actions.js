/**
 * Created by Kevin on 5/03/2017.
 */
import * as types from '../constants/actionsTypes'
import * as endpoints from '../constants/endpoints'
import AuthService from '../services/AuthService'
import userService from '../services/UserService'
import axios from 'axios'
import { browserHistory } from 'react-router'

const authService = new AuthService('mnqs0QjIVlImDGckMWibNAMEvX3WnOnX', 'kevinimpens.eu.auth0.com')

export const searchUsersRecieved = users => ({ type: types.SEARCH_USERS_RECIEVED, users })
export const searchUsers = (query, short) => (function(dispatch){
    let url = endpoints.usersEndpoint
    if(short){
        if(query)
            url = `${endpoints.usersEndpoint}?search=${query}&short`
        else
            url = `${endpoints.usersEndpoint}?short`
    }
    return AuthService.fetch(url,'GET')
            .then((resp)=>{
                console.log(resp.data.response)
                dispatch(searchUsersRecieved(resp.data.response.map((user, index)=>{
                    return user
                })))
            },(err)=>{
                console.log(err)
            })
            .catch((err)=>{
                console.log(err)
            })
})

// Listen to authenticated event from AuthService and get the profile of the user
// Done on every page startup
export function checkLogin() {
    return (dispatch) => {
        // Add callback for lock's `authenticated` event
        authService.lock.on('authenticated', (authResult) => {
            console.log(authResult)
            authService.lock.getProfile(authResult.idToken, (error, profile) => {

                if (error)
                    return dispatch(loginError(error))
                AuthService.setToken(authResult.idToken)
                AuthService.setProfile(profile)
                AuthService.getUserId(profile.nickname)
                    .then((resp)=>{
                        console.log(resp)
                        if(resp.data.response) {
                            AuthService.setId(resp.data.response.id)
                            getPhotos()
                                .then((resp)=>dispatch(photosRevieved(resp.data.response)))
                                .catch((err)=>console.log(err))
                        }
                        else{
                            //there is no user with that name yet, let's create one and start the new profile process
                            userService.createNewUser(profile.nickname)
                                .then((resp)=>{
                                    dispatch(newUserCreated(resp.data.response))
                                })
                                .catch((err)=>{
                                    console.log(err)
                                })
                        }
                            console.log()
                    })
                    .catch((err)=>console.log(err))
                return dispatch(loginSuccess(profile))
            })
        })
        // Add callback for lock's `authorization_error` event
        authService.lock.on('authorization_error', (error) => dispatch(loginError(error)))

        if(AuthService.userIsLoggedIn()){
            if(AuthService.isTokenExpired()){
                browserHistory.push('/login')
            }else{
                getPhotos()
                    .then((resp)=>dispatch(photosRevieved(resp.data.response)))
                    .catch((err)=>console.log(err))
            }
        }
    }
}

export function loginRequest() {
    console.log('login requrested')
    authService.login()
    return {
        type: types.LOGIN_REQUEST
    }
}

export function loginSuccess(profile) {
    browserHistory.push('/')
    return {
        type: types.LOGIN_SUCCESS,
        profile
    }
}

export function loginError(error) {
    return {
        type: types.LOGIN_ERROR,
        error
    }
}

export function logoutSuccess() {
    authService.logout()
    browserHistory.push('/')
    return {
        type: types.LOGOUT_SUCCESS
    }
}

export function newUserCreated(user){
    browserHistory.push('/register')
    return {
        type: types.USER_CREATED,
        user
    }
}

export function photosRevieved(photos){
    return {type: types.PHOTOS_RECIEVED, photos}
}

function getPhotos(){
    return AuthService.fetch(endpoints.allPhotos, 'GET')
}

export function addComment(user_id, photo_id, content){
    return (dispatch)=> {
        AuthService.fetch(`${endpoints.apiEndPoint}/users/${user_id}/photos/${photo_id}/comments`, 'POST', {content})
            .then((resp) => {

                dispatch(commentRecieved(resp.data.response))
            })
            .catch((err)=>{
                console.log(err)
                dispatch(commentErrorRecieved(err))
            })
    }

}
export function getPhotoDetail(user_id,photo_id){
    return (dispatch)=> {
        AuthService.fetch(`${endpoints.apiEndPoint}/users/${user_id}/photos/${photo_id}`,'GET')
            .then((resp) => {
                dispatch(photoDetailRecieved(resp.data.response))
            })
            .catch((err)=>dispatch(photoDetailErrorRecieved(err)))
    }

}

export function commentRecieved(comment){
    return  {type: types.COMMENT_RECIEVED, comment}
}

export function commentErrorRecieved(err){
    return  {type: types.COMMENT_ERROR_RECIEVED, err}
}

export function photoDetailRecieved(photoDetail){
    return {type: types.PHOTO_DETAIL_RECIEVED, photoDetail}
}
export function photoDetailErrorRecieved(err){
    return {type: types.PHOTO_DETAIL_ERROR_RECIEVED, err}
}

export function likePhoto(user_id, photo_id){
    return (dispatch)=> {
        AuthService.fetch(`${endpoints.apiEndPoint}/users/${user_id}/photos/${photo_id}/like`,'POST')
            .then((resp) => {
                dispatch(photoLikeRecieved(resp.data.response))
            })
            .catch((err)=>dispatch(photoLikeErrorRecieved(err)))
    }
}


export function photoLikeRecieved(like){
    return {type: types.PHOTO_LIKE_RECIEVED, like}
}
export function photoLikeErrorRecieved(err){
    return {type: types.PHOTO_LIKE_ERROR_RECIEVED, err}
}

export function getProfile(user_id){
    return (dispatch)=> {
        AuthService.fetch(`${endpoints.apiEndPoint}/users/${user_id}`,'GET')
            .then((resp) => {
                dispatch(profileRecieved(resp.data.response))
            })
            .catch((err)=>dispatch(profileErrorRecieved(err)))
    }
}

export function profileRecieved(profile){
    return {type: types.PROFILE_RECIEVED, profile}
}
export function profileErrorRecieved(err){
    return {type: types.PROFILE_ERROR_RECIEVED, err}
}