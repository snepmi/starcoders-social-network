/**
 * Created by Kevin on 6/03/2017.
 */
import Auth0Lock from 'auth0-lock'
import {usersFindIdEndpoint} from '../constants/endpoints'
import axios from 'axios'
import jwtDecode from 'jwt-decode'

class AuthService{
    constructor(clientId, domain){
        // Configure Auth0
        this.lock = new Auth0Lock(clientId, domain, {
            auth: {
                redirectUrl: 'http://localhost:3000',
                responseType: 'token'
            }
        })

        // binds login functions to keep this context
        this.login = this.login.bind(this)
    }


    static userIsLoggedIn(){
        let idToken = localStorage.getItem('id_token')
        return idToken
    }

    login(){
        console.log('login show')
        this.lock.show()
    }

    logout(){
        // Clear user token and profile data from localStorage
        localStorage.removeItem('id_token')
        localStorage.removeItem('profile')
        localStorage.removeItem('user_id')
    }

    static setToken(idToken) {
        // Saves user token to localStorage
        localStorage.setItem('id_token', idToken)
    }

    static getToken() {
        // Retrieves the user token from localStorage
        return localStorage.getItem('id_token')
    }

    static getProfile() {
        // Retrieves the profile data from localStorage
        const profile = localStorage.getItem('profile')
        return profile ? JSON.parse(localStorage.profile) : {}
    }

    static setProfile(profile) {
        // Saves profile data to localStorage
        localStorage.setItem('profile', JSON.stringify(profile))
        // Triggers profile_updated event to update the UI
    }

    static getId() {
        // Retrieves the user_id from localStorage
        const user_id = localStorage.getItem('user_id')
        return user_id ? user_id : null
    }

    static setId(user_id) {
        // Saves user_id to localStorage
        localStorage.setItem('user_id', user_id)
        // Triggers profile_updated event to update the UI
    }

    static getTokenExpirationDate() {
        const token = AuthService.getToken()
        const decoded = jwtDecode(token)
        if(!decoded.exp) {
            return null
        }
        const date = new Date(0) // The 0 here is the key, which sets the date to the epoch
        date.setUTCSeconds(decoded.exp)
        return date
    }

    static isTokenExpired() {
        const token = AuthService.getToken()
        if (!token) return true
        const date = AuthService.getTokenExpirationDate(token)
        const offsetSeconds = 0
        if (date === null) {
            return false
        }
        return !(date.valueOf() > (new Date().valueOf() + (offsetSeconds * 1000)))
    }

    static getUserId(name){
            return this.fetch(`${usersFindIdEndpoint}?name=${name}`,'GET')

    }

    _checkStatus(response) {
        if (response.status >= 200 && response.status < 300) {
            return response
        } else {
            const error = new Error(response.statusText)
            error.response = response
            throw error
        }
    }

    static fetch(url, method, data){
        // performs api calls sending the required authentication headers
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

        if (this.userIsLoggedIn()){
            headers.Authorization = 'Bearer ' + this.getToken()
            headers.Userid = this.getId()
        }
        // return fetch(url, {
        //     headers: new Headers(headers),
        //     method,
        //     ...options
        // })
        return axios({
            method,
            url,
            headers,
            data
        })
    }
}



export default AuthService