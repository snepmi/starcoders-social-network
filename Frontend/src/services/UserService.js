/**
 * Created by Kevin on 6/03/2017.
 */
import authService from './AuthService'
import * as endpoints from '../constants/endpoints'
import axios from 'axios'

class UserService{


    createNewUser(name, email){
        console.log(name, email)
        return authService.fetch(endpoints.createUser, 'post', {name,email})
    }

}

const userService = new UserService()
export default userService