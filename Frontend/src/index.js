import React from 'react';
import ReactDOM from 'react-dom';
import {Router, browserHistory, Route, IndexRoute} from 'react-router'
import App from './containers/App';

import { Provider } from 'react-redux'
import store from "./reducers/store"

//import components
import Home from './components/Home'
import PhotoDetail from './components/PhotoDetail'
import Login from './components/Login'
import Logout from './components/Logout'
import Register from './components/Registration'
import Profile from './components/Profile'

//import services
import AuthService from './services/AuthService'

import './index.css';

ReactDOM.render(
    <Provider store={store}>
        <div>
            <Router history={browserHistory}>
                <Route path="/" component={App} auth={AuthService} >
                    <IndexRoute component={Home}/>
                    <Route path="/home" component={Home}/>
                    <Route path="/users/:userId/photo/:photoId" component={PhotoDetail}/>
                    <Route path="/profile" component={Profile}/>
                    <Route path="/login" component={Login}/>
                    <Route path="/logout" component={Logout}/>
                    <Route path="/register" component={Register}/>
                </Route>

            </Router>
        </div>

    </Provider>,
    document.getElementById('root')
);
