/**
 * Created by Kevin on 1/03/2017.
 */
const fs = require('fs')
const path = require('path')
const db = require('./models/index.js')
const restify = require('restify')
const jwt = require('restify-jwt')
const config = JSON.parse(fs.readFileSync(path.join(__dirname+'/config.json'), 'utf8'));


const server = restify.createServer()
server.use(restify.bodyParser())
server.use(restify.queryParser())
server.use(restify.CORS({
    credentials: true
}))

server.use(jwt({
    secret: config.auth.secret,
    audience: config.auth.client_id
}));

//https://github.com/restify/node-restify/issues/284
function unknownMethodHandler(req, res) {
    if (req.method.toLowerCase() === 'options') {
        console.log('received an options method request');
        let allowHeaders = ['Accept', 'Accept-Version', 'Content-Type', 'Api-Version', 'Origin', 'X-Requested-With', 'Authorization', 'Userid']; // added Origin & X-Requested-With & **Authorization**

        if (res.methods.indexOf('OPTIONS') === -1) res.methods.push('OPTIONS');

        res.header('Access-Control-Allow-Credentials', true);
        res.header('Access-Control-Allow-Headers', allowHeaders.join(', '));
        res.header('Access-Control-Allow-Methods', res.methods.join(', '));
        res.header('Access-Control-Allow-Origin', req.headers.origin);

        return res.send(200);
    }
    else
        return res.send(new restify.MethodNotAllowedError());
}

server.on('MethodNotAllowed', unknownMethodHandler);

//Set up routes
require('./routes/users')(server)
require('./routes/comments')(server)
require('./routes/photos')(server)
require('./routes/likes')(server)


server.listen(process.env.PORT ||config.server.PORT, function(){
    console.log(`Server started at port ${process.env.PORT ||config.server.PORT}`)
})
