/**
 * Created by Kevin on 1/03/2017.
 */
const restify = require('restify')
const Joi = require('joi')
const {isInt} = require('validator')
const {restError,parseJoiErrors} = require('../helpers/errorHandlers')

const schemaPost = Joi.object().keys({
    name: Joi.string().regex(/^[a-zA-Z0-9_ ]+$/).required(),
    email: Joi.string().email()
})

const schemaPut = Joi.object().keys({
    name: Joi.string().regex(/^[a-zA-Z]+$/),
    email: Joi.string().email(),
    description: Joi.string(),
}).or('name','email', 'description')

const {Users, Photos} = require('../models')

function getUsers(req,res,next){
    const search = req.query.search
    const short = req.query.short
    let query = {}
    //check if there is a search paramaters in the querystring
    if(search){
        //Check if it a valid string
        if(!/^[a-zA-Z0-9_ ]+$/.test(req.query.search)){
            return restError.rest400(next, 'Invalid search string')
        }else{
            //build the where of the query
            query.where = {
                name: {
                    $like: `%${search}%`
                }
            }
        }
    }

    //If the a short response is requested the photos of the user will not be returned
    if(short==undefined){
        query.include = [{
            model: Photos
        }]
    }else{
        //Get only the name and the id
        query.attributes = ['id', 'name']
    }

    //query the db
    Users.findAll(query)
    .then((users) => {
        res.json({status:200, response:users})
    })
    .catch((err)=> {
        console.error(err)
        return restError.rest500(next)
    })
}
function findUserId(req,res,next){
    const name = req.query.name
    let query = {}
    //check if there is a name parameter in the querystring
    if(name){
        //Check if it a valid string
        if(!/^[a-zA-Z0-9_ ]+$/.test(name)){
            return restError.rest400(next, 'Invalid name string')
        }else{
            //build the where of the query
            query.where = {
                name: name
            }
            query.attributes = ['id']
        }
    }else{
        return restError.rest400(next, 'No valid name in querystring')
    }

    //query the db
    Users.findOne(query)
    .then((user) => {
        res.json({status:200, response:user})
    })
    .catch((err)=> {
        console.error(err)
        return restError.rest500(next)
    })
}

function getUser(req,res,next){
    Users.find({
        where: {
            id: req.params.id
        },
        include: [{
            model: Photos
        }]
    })
    .then((user) => {
        res.json({status:200, response:user})
    })
    .catch((err)=> {
        console.error(err)
        return restError.rest500(next)
    })
}

function createUser(req,res,next){
    const {name} = req.body
    const {error} = Joi.validate({name}, schemaPost)
    if(error) return restError.rest400(next,parseJoiErrors(error))
    Users.create({name})
    .then((user) => {
        res.json({status:200, response:{id:user.id, name: user.name}})
    })
    .catch((err)=> {
        console.error(err)
        return restError.rest500(next)
    })
}

function updateUser(req,res,next){

    const {name, email, description} = req.body
    const {error} = Joi.validate({name, email, description}, schemaPut)
    if(error) return restError.rest400(next,parseJoiErrors(error))

    const user_id = req.params.id

    let data = {}
    if(name) data.name = name
    if(email) data.email = email
    if(description) data.description = description

    Users.update(data,{
        where: {
            id: user_id
        }})
        .then((user) => {
            res.json(200,{response:user})
        })
        .catch((err)=> {
            console.error(err)
            return restError.rest500(next)
        })
}


function deleteUser(req,res,next){
    const user_id = req.params.id
    //current user
    const currentUser = req.headers.user_id

    if(user_id!=currentUser) return restError.rest400(next, 'You cannot delete an other user')

    Users.destroy({where: {id:user_id}})
        .then((affectedRows) => {
            //return the first number in the response which represents the affected rows
            return res.json(200,{response:affectedRows})
        })
        .catch((err)=> {
            console.error(err)
            return restError.rest500(next)
        })
}



module.exports = {
    getUsers,
    getUser,
    findUserId,
    createUser,
    updateUser,
    deleteUser
}
