/**
 * Created by Kevin on 1/03/2017.
 */


const Joi = require('joi')
const sequelize = require("sequelize")
const {isInt} = require('validator')
const {restError, parseJoiErrors} = require('../helpers/errorHandlers')

const schemaPost = Joi.object().keys({
    title: Joi.string().regex(/^[a-zA-Z0-9_ ]+$/).required(),
    url: Joi.string().required()
})
const schemaPut = Joi.object().keys({
    title: Joi.string().regex(/^[a-zA-Z]+$/),
    url: Joi.string()
}).or('title','url')

const {Users, Photos, Likes, Comments} = require('../models')

function getPhotosUser(req,res,next){
    Photos.findAll({
        where:{
            user_id: req.params.id
        }
    })
    .then((post) => {
        res.json(200,{response:post})
    })
    .catch((err)=> {
        console.error(err)
        return restError.rest500(next)
    })
}

function getPhotoUser(req,res,next){
    Photos.find({
        where:{
            user_id: req.params.id
        },
        include: [
            {model: Comments, include:[{model:Users,attributes:['name']}]},
            {model: Likes, include:[{model:Users,attributes:['name']}]},
            {model: Users},
        ]

    })
    .then((post) => {
        res.json(200,{response:post})
    })
    .catch((err)=> {
        console.error(err)
        return restError.rest500(next)
    })
}

function getPhotos(req,res,next){
    let currentUser = req.headers.userid
    Photos.findAll({
        where:{
            user_id: {
                $not: currentUser
            }
        },
        include: [
            {model: Likes, include:[{model:Users,attributes:['name']}]},
            {model: Users},
        ]
    })
        .then((post) => {
            res.json(200,{response:post})
        })
        .catch((err)=> {
            console.error(err)
            return restError.rest500(next)
        })
}

function addPhotoUser(req,res,next){
    let {title, url} = req.body
    const {error, value} = Joi.validate({title, url}, schemaPost)
    if(error) return restError.rest400Joi(next,parseJoiErrors(error))

    //Set user id
    let data = {title, url}
    data.user_id = req.params.id
    Photos.create(data)
        .then((post) => {
            res.json(200,{response:post})
        })
        .catch((err)=> {
            console.error(err)
            return restError.rest500(next)
        })
}
function updatePhotoUser(req,res,next){
    //check if the photoId is an int
    if(!isInt(req.params.id)) return restError.rest400(next,'Photo id is not a number')
    let {title, url} = req.body
    const {error} = Joi.validate({title, url}, schemaPut)
    if(error) return restError.rest400Joi(next,parseJoiErrors(error))

    const user_id = req.params.id
    const photoId = req.params.id

    //check if the photo exist
    Photos.findById(photoId)
        .then((resp)=>{
            if(!resp) return restError.rest400(next,'Photo doesn\'t exist')
        })
        .then(()=>Photos.update({title, url, user_id}, {where: {id:photoId}}))
        .then((affectedRows) => {
            //return the first number in the response which represents the affected rows
            return res.json(200,{response:affectedRows[0]})
        })
        .catch((err)=> {
            console.error(err)
            return restError.rest500(next)
        })
}
function deletePhotoUser(req,res,next){
    if(!isInt(req.params.photoId)) return restError.rest400(next,'Photo id is not a number')

    const photoId = req.params.photoId

    Photos.findById(photoId)
        .then((resp)=>{
            if(!resp) return restError.rest400(next,'Photo doesn\'t exist')
                Photos.destroy({where: {id:photoId}})
                .then((affectedRows) => {
                    //return the first number in the response which represents the affected rows
                    return res.json(200,{response:affectedRows})
                })
        })
        .catch((err)=> {
            console.error(err)
            return restError.rest500(next)
        })
}

module.exports = {
    getPhotosUser,
    getPhotoUser,
    getPhotos,
    addPhotoUser,
    updatePhotoUser,
    deletePhotoUser
}