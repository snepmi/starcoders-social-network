/**
 * Created by Kevin on 1/03/2017.
 */

const {Users,Photos,Comments} = require('../models')
const {restError, parseJoiErrors} = require('../helpers/errorHandlers')
const {isInt} = require('validator')
const Joi = require('joi')

const schema = Joi.object().keys({
    content: Joi.string().required()
})


function getCommentsPhoto(req,res,next){
    Comments.findAll({
            where:{
                photo_id: req.params.photoId
            },
            include: [
                Users
            ]

        })
        .then((post) => {
            res.json({status:200, response:post})
        })
        .catch((err)=> {
            console.error(err)
            return restError.rest500(next)
        })
}
function postCommentPhoto(req,res,next){
    let {content} = req.body
    const {error, value} = Joi.validate({content}, schema)
    if(error) return restError.rest400Joi(next,parseJoiErrors(error))

    //Get the current logged in user from the header and validate
    let currentUser = req.headers.userid
    if(!currentUser) return restError.rest400(next, 'No user id in header')
    if(!isInt(currentUser)) return restError.rest400(next, 'Invalid user id in header')
    //Set user id
    let data = {}
    data.user_id = parseInt(currentUser)
    data.photo_id = parseInt(req.params.photoId)
    data.content = content
    Comments.create(data)
        .then((comment) => {
            Users.findById(comment.user_id)
                .then((resp)=>{
                    comment.dataValues.User={
                        name: resp.name
                    }
                    res.json({status:200, response:comment})
                })

        }, (err)=>{
            console.log(err)
            return restError.rest500(next)
        })
        .catch((err)=> {
            console.error(err)
            return restError.rest500(next)
        })
}
function deleteCommentPhoto(req,res,next){
    if(!req.params.commentId) return restError.rest400(next,'No valid comment id')
    if(!isInt(req.params.commentId)) return restError.rest400(next,'Comment id is not a number')

    const photo_id = req.params.photoId
    const comment_id = req.params.commentId

    Comments.findById(comment_id)
        .then((resp)=> {
            if (!resp) return restError.rest400(next,'Comment doesn\'t exist')
            Comments.destroy({where: {id: comment_id, photo_id}})
                .then((affectedRows) => {
                    //return the first number in the response which represents the affected rows
                    return res.json({status: 200, response: affectedRows})
                })
        })
        .catch((err)=> {
            console.error(err)
            return restError.rest500(next)
        })
}

module.exports = {
    getCommentsPhoto,
    postCommentPhoto,
    deleteCommentPhoto
}