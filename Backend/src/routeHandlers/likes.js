/**
 * Created by Kevin on 4/03/2017.
 */
const {Likes} = require('../models')
const {restError} = require('../helpers/errorHandlers')

function toggleLikePhotoUser(req,res,next){
    const photo_id = req.params.photoId
    //user id of the use who like or unlike the post, this id is send through the headers
    const user_id = req.headers.userid
    Likes.find({where: {photo_id}})
        .then((resp)=>{
            if(!resp)
                Likes.create({photo_id, user_id})
                    .then((resppo)=>{
                        res.json(200,{response: {inserted:1,photo_id,user_id}})
                    }, (err)=>{
                        console.log(err)
                        return restError.rest500(next)
                    })
            else
                Likes.destroy({where: {id:resp.getDataValue('id')}})
                    .then((resp)=>res.json(200,{response: {inserted:0,photo_id,user_id}}))
        })
        .catch((err)=>{
            console.log(err)
            return restError.rest500(next)
        })
}

module.exports = {
    toggleLikePhotoUser
}