/**
 * Created by Kevin on 1/03/2017.
 */
module.exports = (sequelize, DataTypes)=> {
    const schema = {
        id: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        title: {
            type: DataTypes.STRING(60),
            allowNull: true
        },
        url: {
            type: DataTypes.STRING(150),
            allowNull: false,
            validation:{
                isUrl: true
            }
        }
    }

    const Photos = sequelize.define('Photos', schema, {

            tableName: 'photos',


        }
    )

    return Photos
}