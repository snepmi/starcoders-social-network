/**
 * Created by Kevin on 1/03/2017.
 */

module.exports = (sequelize, DataTypes)=> {
    const schema = {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(60),
            allowNull: false
        },
        email: {
            type: DataTypes.STRING(60),
            allowNull: true,
            validate:{
                isEmail: true
            }
        },
        description: {
            type: DataTypes.TEXT,
            allowNull: true
        }
    }

    const Users = sequelize.define('Users', schema, {

            tableName: 'users',


        }
    )

    return Users
}