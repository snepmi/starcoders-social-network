/**
 * Created by Kevin on 1/03/2017.
 */

module.exports = (sequelize, DataTypes)=> {
    const schema = {
        id: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        content: {
            type: DataTypes.TEXT,
            allowNull: false
        }
    }

    const Comments = sequelize.define('Comments', schema, {

            tableName: 'comments',


        }
    )

    return Comments
}