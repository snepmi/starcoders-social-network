const path = require('path')
const config = require('../config.json').mysql
const fs = require("fs")
const Sequelize = require("sequelize")

let db = {}

const sequelize = new Sequelize(config.database, config.user, config.password, {
  dialect: "mysql",
  port: config.port,
  host: config.host,
  logging: config.logging,
  maxConcurrentQueries: 100,
  define: {
    underscored: true,
    freezeTableName: false,
    syncOnAssociation: true,
    charset: "utf8",
    collate: "utf8_general_ci",
    timestamps: true
  },
  pool: {
    maxIdleTime: 10000,
    maxConnections: 5,
    minConnections: 0
  }
})

fs.readdirSync(__dirname).filter((file)=> {
  return file.includes(".js") && file !== 'index.js'
}).forEach((file)=> {
  let model = sequelize.import(path.join(__dirname, file))
  db[model.name] = model
})


db.Users.hasMany(db.Photos,{onDelete: 'cascade', hooks:true})
db.Users.hasMany(db.Comments, {onDelete: 'cascade', hooks:true})
db.Users.hasMany(db.Likes, {onDelete: 'cascade', hooks:true})

db.Photos.belongsTo(db.Users)
db.Comments.belongsTo(db.Users)
db.Likes.belongsTo(db.Users)

db.Photos.hasMany(db.Comments, {onDelete: 'cascade', hooks:true})
db.Photos.hasMany(db.Likes)



module.exports = Object.assign({
  sequelize: sequelize,
  Sequelize: Sequelize
}, db)