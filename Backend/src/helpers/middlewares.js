/**
 * Created by Snepmi on 03/03/2017.
 */

const {isInt} = require('validator')
const {Users, Photos} = require('../models')
const {restError} = require('./errorHandlers')
function checkIfUserExist(req,res,next){
    if(!isInt(req.params.id)) return restError.rest400(next,'No valid user id')
    Users.findById(req.params.id)
        .then((resp)=>{
            if(!resp)  return restError.rest404(next,'No user found')
            else next()
        })
        .catch((err)=>{
            console.log(err)
        })
}

function checkIfPhotoExistForUser(req,res,next){
    if(!isInt(req.params.photoId)) return restError.rest400(next,'No valid photo id')
    Photos.findById(req.params.photoId)
        .then((resp)=>{
            if(!resp)  return restError.rest404(next,'No photo found')
            else if(resp.dataValues.user_id!=req.params.id) return restError.rest404(next,`No photo found for user with id: ${req.params.id}`)
            else next()
        })
        .catch((err)=>{
            console.log(err)
        })
}

function checkUserIdHeader(req,res,next){
    if(!req.headers.userid) return restError.rest400(next, 'No user_id in header')
    if(!isInt(req.headers.userid)) return restError.rest400(next, 'Not a valid user_id in header')
    next()
}

module.exports = {checkIfUserExist,checkIfPhotoExistForUser, checkUserIdHeader}