/**
 * Created by Snepmi on 03/03/2017.
 */
var restify = require('restify')
const util = require('util')
//Setup custom restify errors
function JoiValidationError(message) {
    restify.RestError.call(this, {
        restCode: 'BadRequestError',
        statusCode: 400,
        message: message,
        constructorOpt: JoiValidationError
    });
    this.name = 'JoiValidationError';
};
util.inherits(JoiValidationError, restify.RestError);
module.exports = JoiValidationError
