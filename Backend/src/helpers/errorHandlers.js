/**
 * Created by Snepmi on 03/03/2017.
 */
const restify = require('restify')
const JoiValidationError = require('./customErrors')

let restError =  {
    rest404:(next, msg)=>{
        return next(new restify.NotFoundError(msg))
    },
    rest400:(next, msg)=>{
        return next(new restify.BadRequestError(msg))
    },
    rest400Joi:(next, msg)=>{
        return next(new JoiValidationError(msg))
    },
    rest500:(next)=>{
        return next(new restify.InternalServerError ('Something went wrong on our part'))
    }

}

function parseJoiErrors(error){
    const details = error.details
    return details.map((item,index)=>item.message.replace(/"/g,""))
}

module.exports = {restError, parseJoiErrors}

