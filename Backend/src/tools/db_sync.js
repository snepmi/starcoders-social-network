const {sequelize} = require('../models')
const {Category, User} = sequelize.models

sequelize.sync()
.then(()=>{
    console.log('database synced')
})
.catch((err)=>{
    console.log(`Error syncing: ${err}`)
})