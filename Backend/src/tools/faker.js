/**
 * Created by Kevin on 1/03/2017.
 */
const {Users, Photos, Comments} = require('../models')
const faker = require('faker')

    Users.create({
        name: `${faker.name.firstName()} ${faker.name.lastName()}`,
        description: faker.lorem.paragraph(),
        email: faker.internet.email(),
        Photos:[
            {
                title: faker.lorem.words(),
                url: faker.image.imageUrl()
            },
            {
                title: faker.lorem.words(),
                url: faker.image.imageUrl(),
            },
            {
                title: faker.lorem.words(),
                url: faker.image.imageUrl()
            }
        ],

    }, {
        include: [Photos]
    })
        .then((resp) => {
            console.log('inserted fake data')
        })
        .catch((err)=> {
            console.error(err)
        })