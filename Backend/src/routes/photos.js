/**
 * Created by Kevin on 1/03/2017.
 */

const {getPhotosUser, getPhotoUser, getPhotos, addPhotoUser, updatePhotoUser, deletePhotoUser} = require('../routeHandlers/photos')
const {checkIfUserExist, checkIfPhotoExistForUser,checkUserIdHeader} = require('../helpers/middlewares')
module.exports = (server)=>{
    const prefix = '/users/:id'
    server.get(`${prefix}/photos`, checkIfUserExist, getPhotosUser)
    server.get(`${prefix}/photos/:photoId`, [checkIfUserExist, checkIfPhotoExistForUser], getPhotoUser)


    server.get(`/photos`, [checkUserIdHeader], getPhotos)

    server.post(`${prefix}/photos`, checkIfUserExist, addPhotoUser)

    server.put(`${prefix}/photos/:photoId`, checkIfUserExist,updatePhotoUser)

    server.del(`${prefix}/photos/:photoId`, checkIfUserExist, deletePhotoUser)




}