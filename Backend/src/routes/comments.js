/**
 * Created by Kevin on 1/03/2017.
 */
const {getCommentsPhoto, postCommentPhoto,deleteCommentPhoto} = require('../routeHandlers/comments')
const {checkIfUserExist, checkIfPhotoExistForUser, checkUserIdHeader} = require('../helpers/middlewares')

const middleware = [checkIfUserExist,checkIfPhotoExistForUser]

module.exports = (server)=>{
    const base = 'users/:id/photos/:photoId'
    server.get(`${base}/comments`,middleware, getCommentsPhoto)

    server.post(`${base}/comments`,[checkUserIdHeader,...middleware], postCommentPhoto)

    server.del(`${base}/comments/:commentId`,middleware, deleteCommentPhoto)

}