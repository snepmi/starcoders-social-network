/**
 * Created by Kevin on 1/03/2017.
 */
const {getUsers, findUserId, getUser, createUser, updateUser, deleteUser} = require('../routeHandlers/users')
const {checkIfUserExist, checkUserIdHeader} = require('../helpers/middlewares')
module.exports = (server)=>{
    server.get('/users', getUsers)
    server.get('/users/find', findUserId)
    server.get('/users/:id', checkIfUserExist, getUser)

    server.post('/users', createUser)

    server.put('/users/:id', checkIfUserExist, updateUser)

    server.del('/users/:id', [checkIfUserExist,checkUserIdHeader], deleteUser)
}