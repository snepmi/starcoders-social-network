/**
 * Created by Kevin on 4/03/2017.
 */

const {toggleLikePhotoUser} = require('../routeHandlers/likes')
const {checkIfUserExist, checkIfPhotoExistForUser, checkUserIdHeader} = require('../helpers/middlewares')
const middlewares = [checkIfUserExist, checkIfPhotoExistForUser, checkUserIdHeader]
module.exports = (server)=>{
    const prefix = '/users/:id/photos/:photoId'
    server.post(`${prefix}/like`, middlewares, toggleLikePhotoUser)
}